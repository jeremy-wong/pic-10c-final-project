The project is to build a working battleship game. 

A single player console version will be implemented first where the user will play the game with board initialize by the computer. 
The program will display the board and ask the player where to hit repeatedly. 
The game will end until the player destroy all the battleships.

Then the game will be implemented with Qt, where player can interact with mouse clicking.
If time permits, I will try to implement the game in a two player version.