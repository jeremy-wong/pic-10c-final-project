#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <ctime>
#include <cstdlib>
#include "battleship.h"

int main()
{
	Board board;
	int row, col, turn;
	bool flag;
	turn = 1;
	
	board.initBoard();

	cout << "Welcome to the Battleship game!\n"
		<< "You will be asked to enter where you want to shoot at,\n"
		<< "and the game ends when all the boat are sunk.\n"
		<< "O = can be targeted  X = hit  M = miss\n";
	
	board.displayBoard();
	
	while (board.get_boatList() > 0)
	{
		flag = false;
		cout << "Turn: " << turn << "\n";

		while (!flag) {
			cout << "\nEnter the row coordinate: ";
			cin >> row;
			cout << "Enter the column coordinate: ";
			cin >> col;
			flag = board.shooting(row, col);
		}
		board.displayBoard();
		turn++;
	}
	cout << "You win the game! Congratulations!\n";
    
	return 0;
}

