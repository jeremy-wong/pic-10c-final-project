#include "battleship.h"
#include <cstdlib>
#include <ctime>
#include <iostream>


using namespace std;

Boat::Boat(int lgth, char d, vector<int> row, vector<int> col, int hit, string my_name) {
	length = lgth;
	dir = d;
	boatrow = row;
	boatcol = col;
	hits = hit;
	name = my_name;
}

void Boat::setHit() {
	hits++;
}

bool Boat::checkSunk() {
	return hits >= length;
}

void Boat::boatCoordinate() {
	cout << name << " is located at:\n\n";
	for (int i = 0; i < length; i++) {
		cout << "\tGrid [" << boatrow[i] << "] [" << boatcol[i] << "]\n";
	}
}

string Boat::getBoat(int row, int col) {
	for (int i = 0; i < length; i++) {
		if (boatrow[i] == row && boatcol[i] == col) {
			return name;
		}
	}
	return "";
}

//Board class
Board::Board() {
	for (unsigned i = 0; i < 10; i++) {
		for (unsigned j = 0; j < 10; j++) {
			grid[i][j] = 0;
		}
	}
}

int Board::get_boatList() {
	return boatList.size();
}

char Board::getDirection(int num) {
	if (num < 4) { return 'h'; }
	else { return 'v'; }
}

int Board::resetColAndRow(int col, int &row, int size, char dir) {
	switch (size) //Generate random column and row based on boat size so we don't go over the edge of the grid
	{
	case Submarine:
		if (dir == 'h')
		{
			col = rand() % 8;
			row = rand() % 10;
		}
		else
		{
			col = rand() % 10;
			row = rand() % 8;
		}
		break;
	case Destroyer:
		if (dir == 'h')
		{
			col = rand() % 7;
			row = rand() % 10;
		}
		else
		{
			col = rand() % 10;
			row = rand() % 7;
		}
		break;
	case Battleship:
		if (dir == 'h')
		{
			col = rand() % 6;
			row = rand() % 10;
		}
		else
		{
			col = rand() % 10;
			row = rand() % 6;
		}
		break;
	case Carrier:
		if (dir == 'h')
		{
			col = rand() % 5;
			row = rand() % 10;
		}
		else
		{
			col = rand() % 10;
			row = rand() % 5;
		}
	}
	return col;
}

void Board::editGrid(int col, int row, int size, char dir) {
	if (dir == 'h')
	{
		for (int i = 0; i<size; i++)
		{
			grid[row][col] = size;
			col++;
		}
	}
	else if (dir == 'v')
	{
		for (int i = 0; i<size; i++)
		{
			grid[row][col] = size;
			row++;
		}
	}
	else
	{
		cout << "Error!  No direction passed" << endl;
	}
}

void Board::createBoat(int col, int row, int size, char dir, int name) {
	vector<int> row1(size);
	vector<int> col1(size);
	if (dir == 'h')
	{
		for (auto &r : row1) {
			r = row;
		}
		for (auto &c : col1) {
			c = col;
			col++;
		}
	}
	else if (dir == 'v')
	{
		for (auto &r : row1) {
			r = row;
			row++;
		}
		for (auto &c : col1) {
			c = col;
		}
	}
	switch (name)
	{
		case 1:
		{
			Boat carrierBoat(size, dir, row1, col1, 0, "Aircraft Carrier");
			boatList.push_back(carrierBoat);
			return;
		}
		case 2:
		{
			Boat battleshipBoat(size, dir, row1, col1, 0, "Battleship 1");
			boatList.push_back(battleshipBoat);
			return;
		}
		case 3:
		{
			Boat battleship2Boat(size, dir, row1, col1, 0, "Battleship 2");
			boatList.push_back(battleship2Boat);
			return;
		}
		case 4:
		{
			Boat destroyerBoat(size, dir, row1, col1, 0, "Destroyer 1");
			boatList.push_back(destroyerBoat);
			return;
		}
		case 5:
		{
			Boat destroyer2Boat(size, dir, row1, col1, 0, "Destroyer 2");
			boatList.push_back(destroyer2Boat);
			return; 
		}
		case 6:
		{
			Boat destroyer3Boat(size, dir, row1, col1, 0, "Destroyer 3");
			boatList.push_back(destroyer3Boat);
			return;
		}
		case 7:
		{
			Boat submarineBoat(size, dir, row1, col1, 0, "Submarine 1");
			boatList.push_back(submarineBoat);
			return;
		}
		case 8:
		{
			Boat submarine2Boat(size, dir, row1, col1, 0, "Submarine 2");
			boatList.push_back(submarine2Boat);
			return;
		}
		case 9:
		{
			Boat submarine3Boat(size, dir, row1, col1, 0, "Submarine 3");
			boatList.push_back(submarine3Boat);
			return;
		}
		case 10:
		{
			Boat submarine4Boat(size, dir, row1, col1, 0, "Submarine 4");
			boatList.push_back(submarine4Boat);
			return;
		}
	}
}



bool Board::checkSpace(int col, int row, int size, char dir) {
	if (dir == 'v') {
		for (int i = row; i < (row + size); i++) {
			if (grid[i][col] != 0) { return false; }
		}
		return true;
	}
	else{
		for (int i = col; i < (col + size); i++) {
			if (grid[row][i] != 0) { return false; }
		}
		return true;
	}
}

bool Board::setBoat(int size, int name) {
	srand(1);
	int col = 0;
	int row = 0;
	char dir = 'v';
	bool cS = false;

	do	{
		dir = getDirection(rand() % 10);
		col = resetColAndRow(col, row, size, dir);
		cS = checkSpace(col, row, size, dir);
	} while (!cS);
	editGrid(col, row, size, dir);
	createBoat(col, row, size, dir, name);
	return 0;
}

void Board::initBoard() {
	setBoat(Carrier, 1);
	setBoat(Battleship, 2);
	setBoat(Battleship, 3);
	setBoat(Destroyer, 4);
	setBoat(Destroyer, 5);
	setBoat(Destroyer, 6);
	setBoat(Submarine, 7);
	setBoat(Submarine, 8);
	setBoat(Submarine, 9);
	setBoat(Submarine, 10);
	return;
}

void Board::displayBoard() {
	cout << "\n   0|1|2|3|4|5|6|7|8|9\n";
	for (unsigned i = 0; i < 10; i++) {
		cout << i << " |";
		for (unsigned j = 0; j < 10; j++) {
			if (grid[i][j] == 1) {
				cout << "X|";
			}
			else if (grid[i][j] == 9) {
				cout << "M|";
			}
			else {
				cout << "O|";
			}
		}
		cout << "\n";
	}
	cout << "\n";
	return;
}

void Board::displayRealBoard() {
	cout << "   0|1|2|3|4|5|6|7|8|9\n";
	for (unsigned i = 0; i < 10; i++) {
		cout << i << " |";
		for (unsigned j = 0; j < 10; j++) {
			cout << grid[i][j] << "|";
		}
		cout << "\n";
	}
	cout << "\n";
	return;
}

bool Board::shooting(int row, int col) {
	if (col > 9 || col < 0 || row > 9 || row < 0) {
		cout << "invalid target\n";
		return false;
	}
	if (grid[row][col] == 1 || grid[row][col] == 9) {
		cout << "Target already been shot.\n";
		return false;
	}
	else if (grid[row][col] == 0) {
		cout << "Miss.\n";
		grid[row][col] = 9;
		return true;
	}
	else{
		grid[row][col] = 1;
		cout << "Hit!\n";

		string btname = "";
		int index = 0;
		for (auto &boat: boatList) {
			btname = boat.getBoat(row, col);
			if (!btname.empty()) {
				boat.setHit();
				cout << "You hit at " << btname << "!\n";
				if (boat.checkSunk()) {
					cout << "You have sunk " << btname << "!\n";
					boatList.erase(boatList.begin() + index);
				}
				break;
			}
			index++;
		}
		return true;

	}
}