#include <string>
#include <vector>
#include <fstream>


using namespace std;

enum BoatType {Carrier = 5, Battleship = 4,  Destroyer = 3, Submarine = 2};

class Boat {
	public:
		//Constructor
		Boat(int lgth, char d, vector<int> row, vector<int> col, int hit, string name);

		void setHit();
		bool checkSunk();
		void boatCoordinate();
		string getBoat(int row, int col);

	private:
		int length;
		char dir;
		vector<int> boatrow;
		vector<int> boatcol;
		int hits;
		string name;
};

class Board{
	public:
		Board();
		
		friend Boat;

		int get_boatList();
		char getDirection(int num);
		int resetColAndRow(int col, int &row, int size, char dir);
		void editGrid(int col, int row, int size, char dir);
		void createBoat(int col, int row, int size, char dir, int name);

		bool checkSpace(int col, int row, int size, char dir);
		bool setBoat(int size, int name);

		void initBoard();

		void displayBoard();
		void displayRealBoard();

		bool shooting(int row, int col);

	private:
		int grid[10][10];
		vector<Boat> boatList;

};